﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSlider : MonoBehaviour
{

    public float timeRemaining;
    private const float maxTime = 150f;
    public Slider slider;


    private void Start()
    {
     //could have set timeRemaining to equal maxTime here   
    }
    void Update()
    {
        slider.value = CalculateSliderValue();


        if (timeRemaining <= 0)
        {
            timeRemaining = 0;
        }

        else if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
    }

    float CalculateSliderValue()
    {
        return (timeRemaining / maxTime);
    }
}
